
var row = 1;
var addcontainer = document.getElementById("addcontainer")
var updatecontainer = document.getElementById("updatecontainer")
updatecontainer.style.display = "none"
let editingRowId

var add = document.getElementById("add");
add.addEventListener("click", displayDetails)

function displayDetails() {
    var name = document.getElementById("name").value;
    var age = document.getElementById("age").value;
    var num = document.getElementById("num").value;
    var mail = document.getElementById("mail").value;


    if (!name || !age || !num || !mail) {
        alert("please fill all the boxes");
        return;
    }

    if (!isValidEntry(age, num, mail, null)) {
        return;
    }

    var display = document.getElementById("display");
    var newRow = display.insertRow(row);
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    var cell4 = newRow.insertCell(3);
    var cell5 = newRow.insertCell(4);

    cell1.innerHTML = name;
    cell2.innerHTML = age;
    cell3.innerHTML = num;
    cell4.innerHTML = mail;
    cell5.innerHTML = '<button onclick="deleteTableRow(this)">delete</button><button onclick="editTableRow(this)">edit</button>';

    row++;
    clearForm()
}

function isValidEntry(age, num, mail, editingRowId) {
    const table = document.getElementById("display");

    for (let i = 1; i < table.rows.length; i++) {
        const EmpNum = (table.rows[i].cells[2].innerHTML)
        const EmpMail = (table.rows[i].cells[3].innerHTML)

        if (i === editingRowId) {
            continue;
        }
        if (num === EmpNum || mail === EmpMail) {
            alert("duplicate entry");
            return false;
        }

    }
    if (num.length !== 10) {
        alert("please enter valid number");
        return false;
    }
    if (!validateEmail(mail)) {
        alert("please enter valid email");
        return false;
    }
    if (isNaN(age) || age.length > 3 || Number(age) <= 0) {
        alert("please enter valid age");
        return false;
    }
    return true;
}
function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function deleteTableRow(button) {
    var index, table = document.getElementById("display");

    const tablerow = button.closest("tr")
    index = tablerow.rowIndex;
    for (var i = 1; i < table.rows.length; i++) {

        if (index === i) {
            var c = confirm("do you want to delete this row");
            if (c === true) {
                table.deleteRow(index);
                cancel()
                row--;
            }
            break;
        }
    }
}
function editTableRow(button) {
    addcontainer.style.display = "none"
    updatecontainer.style.display = "block"
    var rIndex, table = document.getElementById("display");

    const tablerow = button.closest("tr")
    rIndex = tablerow.rowIndex;

    editingRowId = rIndex

    const EmpName = (table.rows[rIndex].cells[0].innerHTML)
    const EmpAge = (table.rows[rIndex].cells[1].innerHTML)
    const EmpNum = (table.rows[rIndex].cells[2].innerHTML)
    const EmpMail = (table.rows[rIndex].cells[3].innerHTML)

    document.getElementById("name").value = EmpName;
    document.getElementById("age").value = EmpAge;
    document.getElementById("num").value = EmpNum;
    document.getElementById("mail").value = EmpMail;

}
function updateEmp(button) {
    console.log("saving succes", editingRowId)

    var table = document.getElementById("display");


    var name = document.getElementById("name").value;
    var age = document.getElementById("age").value;
    var num = document.getElementById("num").value;
    var mail = document.getElementById("mail").value;

    if (!isValidEntry(age, num, mail, editingRowId)) {
        return;
    }

    (table.rows[editingRowId].cells[0].innerHTML) = name;
    (table.rows[editingRowId].cells[1].innerHTML) = age;
    (table.rows[editingRowId].cells[2].innerHTML) = num;
    (table.rows[editingRowId].cells[3].innerHTML) = mail;
    console.log(table.rows[editingRowId])
}

function cancel() {
    addcontainer.style.display = "block"
    updatecontainer.style.display = "none"
    clearForm()
}

function clearForm() {
    document.getElementById("name").value = "";
    document.getElementById("age").value = "";
    document.getElementById("num").value = "";
    document.getElementById("mail").value = "";
}
